export const categories = [
    'Electronics',
    'Clothing',
    'Home & Furniture',
    'Books',
    'Toys & Games',
    'Beauty',
    'Sports & Outdoors',
    // Add more categories as needed
  ];
  