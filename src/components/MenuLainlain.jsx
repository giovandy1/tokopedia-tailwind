import React from 'react'

const MenuLainlain = () => {
  return (
    <div className='container mx-10  h-[400px] '>
        <div className=''>
          <h1 className='font-bold text-2xl'>Lain - lain</h1>
        </div>
        <div className=' px-4 grid grid-cols-5 my-6'>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/e06cc5c0-ea69-4c01-abde-19575afcb4e6.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Belanja di Kotamu</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/5/28/9c2a0cf7-a1ec-4635-88fd-7b3f77174868.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Tokopedia Clean</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/5/28/b59a7fc6-b889-4af3-8844-67da84804509.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Pasang Internet & TV Kabel</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2023/6/21/0daddcd3-d739-4fbe-bcec-541fdda3ccc7.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Tukar Tambah</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2023/6/21/4a36b31b-9ad4-438f-aaaf-2cdd26233eca.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Tokopedia Affiliate</p>
            </a>   
        </div>
    </div>
  )
}

export default MenuLainlain