import React from 'react'

const ContentCategories = ({ activeTab }) => {
    return (
        <div className='px-4'>
            {activeTab === 1 &&
                <div class="grid grid-cols-3 gap-2">
                    <div className='py-2'>

                        <label htmlFor="country" className="block text-xs font-medium leading-6 text-gray-900">
                            Jenis Produk Listrik
                        </label>
                        <div className="mt-2 mb-2">
                            <select
                                id="country"
                                name="country"
                                autoComplete="country-name"
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300"
                            >
                                <option>Token Listrik</option>
                                <option>Tagihan Listrik</option>
                                <option>PLN Non-Taglis</option>
                            </select>
                        </div>
                    </div>
                    <div className='py-2'>
                        <label htmlFor="numberID" className="block text-xs font-medium leading-6 text-gray-900">
                            No. Meter/ID Pel
                        </label>
                        <div className="mt-2">
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300  ">
                                <input
                                    type="text"
                                    name="numberID"
                                    id="numberID"
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 "
                                    placeholder="masukkan Nomor"
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className='py-2'>
                            <label htmlFor="price" className="block text-xs font-medium leading-6 text-gray-900">
                                Jenis Produk Listrik
                            </label>
                            <div className="mt-2 mb-2 ">
                                <select
                                    id="price"
                                    name="price"
                                    autoComplete="country-name"
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300"
                                >
                                    <option value="1">Rp.20.000</option>
                                    <option value="2">Rp.50.000</option>
                                    <option value="3">Rp.150.0000</option>
                                    <option value="4">Rp.200.0000</option>
                                    <option value="5">Rp.250.0000</option>
                                    <option value="6">Rp.300.0000</option>
                                    <option value="7">Rp.500.0000</option>
                                    <option value="8">Rp.100.0000</option>
                                </select>
                            </div>

                        </div>

                    </div>
                </div>}
            {activeTab === 2 && <div class="grid grid-cols-3 gap-2">
                <div className='py-2'>

                    <label htmlFor="country" className="block text-xs font-medium leading-6 text-gray-900">
                        Jenis Produk Listrik
                    </label>
                    <div className="mt-2 mb-2">
                        <select
                            id="country"
                            name="country"
                            autoComplete="country-name"
                            className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300"
                        >
                            <option>Token Listrik</option>
                            <option>Tagihan Listrik</option>
                            <option>PLN Non-Taglis</option>
                        </select>
                    </div>
                </div>
                <div className='py-2'>
                    <label htmlFor="numberID" className="block text-xs font-medium leading-6 text-gray-900">
                        No. Meter/ID Pel
                    </label>
                    <div className="mt-2">
                        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300  ">
                            <input
                                type="text"
                                name="numberID"
                                id="numberID"
                                className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 "
                                placeholder="masukkan Nomor"
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <div className='py-2'>
                        <label htmlFor="price" className="block text-xs font-medium leading-6 text-gray-900">
                            Jenis Produk Listrik
                        </label>
                        <div className="mt-2 mb-2 ">
                            <select
                                id="price"
                                name="price"
                                autoComplete="country-name"
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300"
                            >
                                <option value="1">Rp.20.000</option>
                                <option value="2">Rp.50.000</option>
                                <option value="3">Rp.150.0000</option>
                                <option value="4">Rp.200.0000</option>
                                <option value="5">Rp.250.0000</option>
                                <option value="6">Rp.300.0000</option>
                                <option value="7">Rp.500.0000</option>
                                <option value="8">Rp.100.0000</option>
                            </select>
                        </div>

                    </div>

                </div>
            </div>}
            {activeTab === 3 &&
                <div class="grid grid-cols-3 gap-2">
                    <div className='py-2'>

                        <label htmlFor="country" className="block text-xs font-medium leading-6 text-gray-900">
                            Jenis Produk Listrik
                        </label>
                        <div className="mt-2 mb-2">
                            <select
                                id="country"
                                name="country"
                                autoComplete="country-name"
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300"
                            >
                                <option>Token Listrik</option>
                                <option>Tagihan Listrik</option>
                                <option>PLN Non-Taglis</option>
                            </select>
                        </div>
                    </div>
                    <div className='py-2'>
                        <label htmlFor="numberID" className="block text-xs font-medium leading-6 text-gray-900">
                            No. Meter/ID Pel
                        </label>
                        <div className="mt-2">
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300  ">
                                <input
                                    type="text"
                                    name="numberID"
                                    id="numberID"
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 "
                                    placeholder="masukkan Nomor"
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className='py-2'>
                            <label htmlFor="price" className="block text-xs font-medium leading-6 text-gray-900">
                                Jenis Produk Listrik
                            </label>
                            <div className="mt-2 mb-2 ">
                                <select
                                    id="price"
                                    name="price"
                                    autoComplete="country-name"
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300"
                                >
                                    <option value="1">Rp.20.000</option>
                                    <option value="2">Rp.50.000</option>
                                    <option value="3">Rp.150.0000</option>
                                    <option value="4">Rp.200.0000</option>
                                    <option value="5">Rp.250.0000</option>
                                    <option value="6">Rp.300.0000</option>
                                    <option value="7">Rp.500.0000</option>
                                    <option value="8">Rp.100.0000</option>
                                </select>
                            </div>

                        </div>

                    </div>
                </div>}
            {activeTab === 4 &&
                <div class="grid grid-cols-3 gap-2">
                    <div className='py-2'>

                        <label htmlFor="country" className="block text-xs font-medium leading-6 text-gray-900">
                            Jenis Produk Listrik
                        </label>
                        <div className="mt-2 mb-2">
                            <select
                                id="country"
                                name="country"
                                autoComplete="country-name"
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300"
                            >
                                <option>Token Listrik</option>
                                <option>Tagihan Listrik</option>
                                <option>PLN Non-Taglis</option>
                            </select>
                        </div>
                    </div>
                    <div className='py-2'>
                        <label htmlFor="numberID" className="block text-xs font-medium leading-6 text-gray-900">
                            No. Meter/ID Pel
                        </label>
                        <div className="mt-2">
                            <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300  ">
                                <input
                                    type="text"
                                    name="numberID"
                                    id="numberID"
                                    className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 "
                                    placeholder="masukkan Nomor"
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className='py-2'>
                            <label htmlFor="price" className="block text-xs font-medium leading-6 text-gray-900">
                                Jenis Produk Listrik
                            </label>
                            <div className="mt-2 mb-2 ">
                                <select
                                    id="price"
                                    name="price"
                                    autoComplete="country-name"
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300"
                                >
                                    <option value="1">Rp.20.000</option>
                                    <option value="2">Rp.50.000</option>
                                    <option value="3">Rp.150.0000</option>
                                    <option value="4">Rp.200.0000</option>
                                    <option value="5">Rp.250.0000</option>
                                    <option value="6">Rp.300.0000</option>
                                    <option value="7">Rp.500.0000</option>
                                    <option value="8">Rp.100.0000</option>
                                </select>
                            </div>

                        </div>

                    </div>
                </div>}
        </div>
    )
}

export default ContentCategories