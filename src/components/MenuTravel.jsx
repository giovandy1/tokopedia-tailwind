import React from 'react'

const MenuTravel = () => {
  return (
    <div className='container mx-10  h-[400px] '>
    <div className=''>
      <h1 className='font-bold text-2xl'>Travel & Entertainment</h1>
    </div>
    <div className=' px-4 grid grid-cols-5 my-6'>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/c1132ce8-de47-48cd-969b-417dea7c6071.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Food & Voucher</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/71c1bbcd-2fa1-4292-866d-38a0f1115ee9.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Hiburan </p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/e5ab9dff-5800-4fd8-a428-2c59b5789de8.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Hotel</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/98fcff91-d906-4933-9294-0382d825bb37.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>M.Tix XXI</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/04fc71a2-9670-4570-8bff-225f03846938.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Streaming </p>
        </a>  
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/89705a1e-f16a-4f86-be48-b713560f3fcb.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Tiket Event</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/35fe35ad-dc1d-4f88-93be-bf09a92580ed.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Tiket Kereta Api </p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/69a9c031-9405-433d-a562-9a790d26e6ef.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Tiket Pesawat</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/4dd52ab0-c8ff-45f3-a83d-01642b834b2a.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Voucher Games</p>
        </a> 
    </div>
</div>
  )
}

export default MenuTravel