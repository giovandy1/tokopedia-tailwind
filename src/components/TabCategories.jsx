import React, { useState } from 'react'
import ProductListTab from './ProductListTab';
const TabCategories = () => {
    const [activeTab, setActiveTab] = useState(1);

    const handleTabClick = (tabNumber) => {
        setActiveTab(tabNumber);
    };
    return (
        <div className='my-8 '>
            <div className='bg-gray-100 bottom-4 h-2'>
            </div>
            <div className=''>
                    <div className=' grid grid-cols-5 gap-4 my-8 sticky top-[100px] z-40 bg-white pt-8 container mx-10 px-10   active:shadow-lg  '>
                        <div
                            className={`cursor-pointer p-2 ${activeTab === 1 ? 'bg-purple-500 text-white rounded-md  h-16 font-bold sticky top-0 ' : 'bg-purple-500 text-white rounded-md  h-16 font-bold  '
                                }`}
                            onClick={() => handleTabClick(1)}
                        >
                            For You
                        </div>
                        <div
                            className={`cursor-pointer p-2 ${activeTab === 2 ? 'bg-green-500 text-white rounded-md  h-16 font-bold ' : 'bg-green-500 text-white rounded-md  h-16 font-bold '
                                }`}
                            onClick={() => handleTabClick(2)}
                        >
                            Beli Lokal
                        </div>
                        <div
                            className={`cursor-pointer p-2 ${activeTab === 3 ? 'bg-orange-300 text-white rounded-md  h-16 font-bold ' : 'bg-orange-500 text-white rounded-md  h-16 font-bold '
                                }`}
                            onClick={() => handleTabClick(3)}
                        >
                            Bumbu & Bahan Masakan
                        </div>
                        <div
                            className={`cursor-pointer p-2 ${activeTab === 4 ? 'bg-green-500 text-white rounded-md  h-16 font-bold ' : 'bg-green-500 text-white rounded-md  h-16 font-bold '
                                }`}
                            onClick={() => handleTabClick(4)}
                        >
                            Perlengkapan Ibadah
                        </div>
                        <div
                            className={`cursor-pointer p-2 ${activeTab === 5 ? 'bg-purple-500 text-white rounded-md  h-16 font-bold ' : 'bg-purple-500 text-white rounded-md  h-16 font-bold '
                                }`}
                            onClick={() => handleTabClick(5)}
                        >
                            Susu & Olahan Susu
                        </div>
                    </div>
                    <div className=" container mx-10 px-10">
                    <ProductListTab activeTab={activeTab} />
               
                </div>
                
            </div>
        </div>
    )
}

export default TabCategories