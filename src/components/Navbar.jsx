import React,{useRef} from 'react'
import phone from '../assets/phone.svg'
import cart from '../assets/cart.svg';
import bell from '../assets/bell.svg';
import envelope from '../assets/envelope.svg';
import location from '../assets/location.svg';
import { Popover } from '@headlessui/react'
import MegaMenu from './MegaMenu';


const Navbar = () => {
    const megaMenuButton = useRef();
    return (
        <div className=' sticky top-0 z-50 shadow-sm  bg-white '>
            <div className='mx-auto bg-slate-200   '>
                <div class="flex justify-between px-4">
                    <div className='flex text-xs text-gray-500 m-2'>
                        <img src={phone} alt="phone" />
                        <div className='px-2'>Download Tokopedia App</div>
                    </div>
                    <div className='flex text-gray-500 text-xs m-2'>
                        <ul className='flex'>
                            <li className='px-4'>Tentang Tokopedia</li>
                            <li className='px-4'>Mitra Tokopedia</li>
                            <li className='px-4'>Pusat Edukasi</li>
                            <li className='px-4'>Promo</li>
                            <li className='px-4'>Tokopedia Care</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <div className='grid grid-cols-9 gap-4 px-4 m-2'>
                    <div className='col-span-1 py-2'>
                        <img src="https://ecs7.tokopedia.net/assets-tokopedia-lite/v2/zeus/production/e5b8438b.svg" alt="" />
                    </div>
                    <div className='col-span-1 m-2'>
                    <Popover className="relative">
            <Popover.Button 
             ref={megaMenuButton}
             className="inline-flex items-center gap-x-1 text-sm p-2 rounded-md leading-6 text-gray-500 hover:bg-gray-200 border-none focus:outline-none"
             onMouseEnter={() => megaMenuButton.current.click()}
             onMouseLeave={() => megaMenuButton.current.click()}>
              <span className="hover:bg-gray-200 border-none focus:outline-none " aria-hidden="true">
                Kategori
              </span>
            </Popover.Button>
            <MegaMenu />
          </Popover>
                     
                    </div>
                    <div className='col-span-4 py-2 '>
                        <input
                            type="text"
                            className="block w-full px-4 py-1.5 text-black bg-white border rounded-md focus:outline-none "
                            placeholder="Cari di Tokopedia"
                        />
                    </div>
                    <div className='col-span-1 py-2 m-2 '>
                        <div className='flex'>
                            <img src={cart} alt="cart" className='px-2' />
                            <img src={bell} alt="notif" className='px-2' />
                            <img src={envelope} alt="message" className='px-2' />
                            <div className='border-r-2 h-[20px] border-gray-300 pl-8'></div>
                        </div>
                    </div>
                    <div className='col-span-2 py-2'>
                        <div className='flex'>
                            <div className='flex px-4'>
                                <img src="https://images.tokopedia.net/img/cache/215-square/GAnVPX/2020/10/15/a183a3b4-aaaa-4436-9537-9543b20914c2.jpg" alt="profil-toko" className='h-8 w-8 rounded-full' />
                                <p className='px-2'>GioGolan</p>
                            </div>
                            <div className='flex px-4'>
                                <img src="https://images.tokopedia.net/img/cache/300/user-1/2019/7/28/20146059/20146059_f7dc9206-0ef7-4c7e-b888-cd3e5ad28845.jpg" alt="profil" className='h-8 w-8 rounded-full' />
                                <p className='px-2'>Gio</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div className='grid grid-cols-9 gap-1 px-4 '>
                <div className='col-span-2 px-2'></div>
                <div className='col-span-4 px-2 '>
                    <div className='flex text-gray-500 '>
                        <ul className='flex text-xs'>
                            <li className='px-2'>RX6800</li>
                            <li className='px-2'>I3 100105</li>
                            <li className='px-2'>Tote bag</li>
                            <li className='px-2'>Iphone 15 Pro</li>
                            <li className='px-2'>Neca Mcfarlane Dc</li>
                            <li className='px-2'> Rtx 3090</li>
                        </ul>
                    </div>
                </div>
                <div className='col-span-3 px-2 pb-2'>
                    <div className='flex px-4 text-xs'>
                        <img src={location} alt="location" className='h-3.5 w-3.5' />
                        <p className='px-2 text-gray-500 '>Dikirim ke <span className='font-semibold  text-black'> Rumah no.36 warna hijau Gio Fandi H Nainggolan</span> </p>
                    </div>

                </div>
            </div>
            <hr />
        </div>
    )
}

export default Navbar