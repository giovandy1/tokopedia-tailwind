import React from 'react'

const MenuTopUp = () => {
  return (
    <div className='container mx-10  h-[400px] '>
        <div className=''>
          <h1 className='font-bold text-2xl'>Top Up</h1>
        </div>
        <div className=' px-4 grid grid-cols-5 my-6'>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/9cbbf2e1-3584-4e1b-9b70-da6d00b11b8c.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Token Listrik</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/6/852f2f1c-64c2-4b9e-8a6c-491db49c9d8d.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Paket Data</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/ab005ba6-4964-4baa-ad06-949e334d8aed.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Pulsa</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/ccfc27c3-be9d-4e66-833e-b4d89a99ba85.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Roaming</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/57bee7ef-9b65-45f4-95a5-e2bacc54a70d.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Uang Elektronik</p>
            </a>   
        </div>
    </div>
  )
}

export default MenuTopUp