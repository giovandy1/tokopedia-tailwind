import React, { Component } from "react";
import Slider from "react-slick";
import music from "../assets/music.svg";

export default class CategoriesProductCarousel extends Component {
    render() {
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 6,
            slidesToScroll: 2,
            initialSlide: 0,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        initialSlide: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };
        return (
            <div className="py-8">
                <Slider {...settings}>
                    <div className="mx-2 ">
                        <div className="bg-white border-2 px-1 py-1 mx-2 rounded-full border-gray-200">
                            <div className="flex ">
                                <img src="https://images.tokopedia.net/img/cache/160-square/iEWsxH/2023/7/4/4cd71d08-49a1-4c8c-9274-54066ab5b449.png" alt="music" className="w-6 h-6" />
                                <p className="text-sm mx-2">Belanja 2 Jam Tiba</p>
                            </div>
                        </div>
                    </div>
                    <div className="mx-2 ">
                        <div className="bg-white border-2 px-1 py-1 mx-2 rounded-full border-gray-200">
                            <div className="flex ">
                                <img src={music} alt="music" className="w-6 h-6" />
                                <p className="text-sm mx-2">Top-Up Tagihan</p>
                            </div>
                        </div>
                    </div> <div className="mx-2 ">
                        <div className="bg-white border-2 px-1 py-1 mx-2 rounded-full border-gray-200">
                            <div className="flex ">
                                <img src={music} alt="music" className="w-6 h-6" />
                                <p className="text-sm mx-2">Top-Up Tagihan</p>
                            </div>
                        </div>
                    </div> <div className="mx-2 ">
                        <div className="bg-white border-2 px-1 py-1 mx-2 rounded-full border-gray-200">
                            <div className="flex ">
                                <img src={music} alt="music" className="w-6 h-6" />
                                <p className="text-sm mx-2">Top-Up Tagihan</p>
                            </div>
                        </div>
                    </div> <div className="mx-2 ">
                        <div className="bg-white border-2 px-1 py-1 mx-2 rounded-full border-gray-200">
                            <div className="flex ">
                                <img src={music} alt="music" className="w-6 h-6" />
                                <p className="text-sm mx-2">Top-Up Tagihan</p>
                            </div>
                        </div>
                    </div> <div className="mx-2 ">
                        <div className="bg-white border-2 px-1 py-1 mx-2 rounded-full border-gray-200">
                            <div className="flex ">
                                <img src={music} alt="music" className="w-6 h-6" />
                                <p className="text-sm mx-2">Top-Up Tagihan</p>
                            </div>
                        </div>
                    </div> <div className="mx-2 ">
                        <div className="bg-white border-2 px-1 py-1 mx-2 rounded-full border-gray-200">
                            <div className="flex ">
                                <img src={music} alt="music" className="w-6 h-6" />
                                <p className="text-sm mx-2">Top-Up Tagihan</p>
                            </div>
                        </div>
                    </div> <div className="mx-2 ">
                        <div className="bg-white border-2 px-1 py-1 mx-2 rounded-full border-gray-200">
                            <div className="flex ">
                                <img src={music} alt="music" className="w-6 h-6" />
                                <p className="text-sm mx-2">Top-Up Tagihan</p>
                            </div>
                        </div>
                    </div>

                    
                </Slider>
            </div>
        );
    }
}
