import React, { Component } from "react";
import Slider from "react-slick";
import ProgressBar from "@ramonak/react-progress-bar";
export default class ProductCarousel extends Component {

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 6,
      slidesToScroll: 6
    };
    const products = [
      {
        id: 1,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://images.tokopedia.net/img/cache/300-square/VqbcmM/2022/4/18/37b1e1a6-fc14-4760-a37b-37870e4439ea.png',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp1.900',
        pricenormal: 'Rp50.900',
        rating: '5',
        discount: '96%',
        place: 'Jakarta Selatan',
      },
      {
        id: 2,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://images.tokopedia.net/img/cache/300-square/VqbcmM/2023/11/25/869f0eb2-3830-424f-9e41-11d3ec48027c.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp2.900',
        rating: '4.8',
        place: 'Jakarta Utara',
      },
      {
        id: 3,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        pricenormal: 'Rp50.900',
        rating: '4.2',
        // coupon:'Casback 3,2rb',
        place: 'Jakarta Timur',
      },
      {
        id: 4,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '4.5',
        place: 'Depok',
      },
      {
        id: 5,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '5',
        place: 'Medan',
      },
      {
        id: 6,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '4.9',
        place: 'Jakarta Utara',
      },
      {
        id: 7,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '4.2',
        place: 'Semarang',
      },
      {
        id: 8,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '3.7',
        place: 'Bandung',
      },
      {
        id: 9,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '4',
        place: 'Jakarta Pusat',
      },
      {
        id: 10,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '4.2',
        place: 'Jakarta Pusat',
      },
      {
        id: 11,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '4.8',
        place: 'Jakarta Pusat',
      },
      {
        id: 12,
        name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
        href: '#',
        imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
        imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
        price: 'Rp38.900',
        rating: '4.1',
        place: 'Jakarta Pusat',
      },
    ]
    return (
      <div className="mx-4">
        <Slider {...settings}>
          {products.map(product => (
            <div className="px-3 mx-2">
              <div key={product.id} className="group relative rounded-md bg-white shadow-md my-4">
                <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-auto">
                  <img
                    src={product.imageSrc}
                    alt={product.imageAlt}
                    className="h-44 w-full object-cover object-center lg:h-44 lg:w-full"
                  />
                </div>
                <div className="mt-1 flex justify-between p-2">
                  <div>
                    <h3 className="text-sm py-1 text-gray-700 line-clamp-2">
                      <a href={product.href}>
                        <span aria-hidden="true" className="absolute inset-0" />
                        {product.name}
                      </a>

                    </h3>
                    <p className="text-sm font-bold text-gray-900">{product.price}  </p>

                    <div className='flex'>
                      <p className='text-xs px-1 text-gray-400 w-[100px] rounded-md '><span className="line-through">{product.pricenormal}</span><span className="text-red-500"> {product?.discount} </span></p>
                    </div>
                    <div className="py-1">
                      <ProgressBar
                        completed={80}
                        height={5}
                        bgColor={'#FF0000'} 
                        isLabelVisible={false}
                      />
                    </div>
                    <div>
                      <p className="text-[10px] font-semibold text-gray-400">Segera Habis</p>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}