import React from 'react'

const MenuHalalCorner = () => {
  return (
    <div className='container mx-10  h-[400px] '>
        <div className=''>
          <h1 className='font-bold text-2xl'>Halal Corner</h1>
        </div>
        <div className=' px-4 grid grid-cols-5 my-6'>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2023/3/21/3db931a5-8285-43bd-9983-00208024003f.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Wakaf</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/3/25/fbb85b4c-d3e9-41bd-941c-03775b7c17e8.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Donasi</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2023/3/21/a15d6932-bc30-430b-a5e0-cc88961e98c5.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Fidyah</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2023/3/21/ae9eed42-489b-42b9-941c-2772605b68e3.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Zakat Maal</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/9/6/4e424f6d-944b-4008-9e67-1aea1dce41f5.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Reksadana Syahriah</p>
            </a>   
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/3/25/7083ed4b-1c14-404d-b8aa-4fbfb333f236.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Zakat</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/9/6/454c6ed1-fbe7-42fc-bb67-a73a837c9056.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Kecantikan Halal</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/9/6/1989a788-ecc7-4c5b-bcb9-68e1e16884fd.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Kesehatan Halal</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/9/6/66cbc2a0-e24f-41be-bdec-a4661d8320bf.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Makanan & Minuman Halal</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/5/28/feb2229f-eed5-462d-8c0c-5494264a1904.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Fashion Muslim</p>
            </a>   
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/9/6/43c3f6ab-e2e3-4e8f-8b78-a554dcd7bae4.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Perlengkapan Ibadah</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/9/6/788ab547-dc66-4f12-8da1-cd89ab819976.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Al Qur'an</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/9/6/84c1e0d4-26cc-4b68-a2cb-d753e2c84b91.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Jadwal Sholat</p>
            </a>
            <a href='#' className='flex py-2'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/6/9/38833552-8378-489e-84f3-877d9dfdda2f.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Qurban</p>
            </a>  
        </div>
    </div>
  )
}

export default MenuHalalCorner