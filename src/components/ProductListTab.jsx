import React from 'react'
import start from '../assets/start.svg';

const ProductListTab = ({ activeTab }) => {
    const products = [
        {
          id: 1,
          name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
          href: '#',
          imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
          imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
          price: 'Rp38.900',
          rating: '5',
          place:'Jakarta Selatan',
        },
        {
            id: 2,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4.8',
            place:'Jakarta Utara',
          },
          {
            id: 3,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4.2',
            coupon:'Casback 3,2rb',
            place:'Jakarta Timur',
          },
          {
            id: 4,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4.5',
            place:'Depok',
          },
          {
            id: 5,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '5',
            place:'Medan',
          },
          {
            id: 6,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4.9',
            place:'Jakarta Utara',
          },
          {
            id: 7,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4.2',
            place:'Semarang',
          },
          {
            id: 8,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '3.7',
            place:'Bandung',
          },
          {
            id: 9,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4',
            place:'Jakarta Pusat',
          },
          {
            id: 10,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4.2',
            place:'Jakarta Pusat',
          },
          {
            id: 11,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4.8',
            place:'Jakarta Pusat',
          },
          {
            id: 12,
            name: 'Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih',
            href: '#',
            imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
            imageAlt: "Front of men's Ellipsesinc - Kaos Oversize Pria Wanita Polos - Putih in black.",
            price: 'Rp38.900',
            rating: '4.1',
            place:'Jakarta Pusat',
          },
    ]
  return (
    <div>
    {activeTab === 1 &&
        <div className="mt-6 grid grid-cols-2 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-6 xl:gap-x-8">
        {products.map((product) => (
          <div key={product.id} className="group relative rounded-md bg-white shadow-md">
            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-auto">
              <img
                src={product.imageSrc}
                alt={product.imageAlt}
                className="h-44 w-full object-cover object-center lg:h-44 lg:w-full"
              />
            </div>
            <div className="mt-1 flex justify-between p-2">
              <div>
                <h3 className="text-sm py-1 text-gray-700 line-clamp-2">
                  <a href={product.href}>
                    <span aria-hidden="true" className="absolute inset-0" />
                    {product.name}
                  </a>
                  
                </h3>
                <p className="text-sm py-1 font-bold text-gray-900">{product.price}</p>
                <div className='py-1'>
                  <p className='text-xs px-1 bg-green-300 text-green-600 w-[100px] rounded-md'>{product.coupon}</p>
                </div>
                  <div className='flex'>
                      <img src="https://images.tokopedia.net/img/goldmerchant/pm_activation/badge/Power%20Merchant%20Pro@2x.png" alt="iconPower" className='w-4 h-4' />
                      <p className='text-sm mx-1'>{product.place}</p>
                  </div>
                <div className='flex py-1'>
                  <img src={start} className='bg-yellow w-4 h-4 mt-1' alt="ratingIcon" />
                <p className="mt-1 text-sm mx-1 text-gray-500">{product.rating} <span> |</span> <span>{product.terjual}Terjual</span></p>
                </div>
              
                
              </div>
            
            </div>
          </div>
        ))}
        </div>}

        {activeTab === 2 &&
        <div className="mt-6 grid grid-cols-2 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-6 xl:gap-x-8">
        {products.map((product) => (
          <div key={product.id} className="group relative rounded-md bg-white shadow-md">
            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-auto">
              <img
                src={product.imageSrc}
                alt={product.imageAlt}
                className="h-44 w-full object-cover object-center lg:h-44 lg:w-full"
              />
            </div>
            <div className="mt-1 flex justify-between p-2">
              <div>
                <h3 className="text-sm py-1 text-gray-700 line-clamp-2">
                  <a href={product.href}>
                    <span aria-hidden="true" className="absolute inset-0" />
                    {product.name}
                  </a>
                  
                </h3>
                <p className="text-sm py-1 font-bold text-gray-900">{product.price}</p>
                <div className='py-1'>
                  <p className='text-xs px-1 bg-green-300 text-green-600 w-[100px] rounded-md'>{product.coupon}</p>
                </div>
                  <div className='flex'>
                      <img src="https://images.tokopedia.net/img/goldmerchant/pm_activation/badge/Power%20Merchant%20Pro@2x.png" alt="iconPower" className='w-4 h-4' />
                      <p className='text-sm mx-1'>{product.place}</p>
                  </div>
                <div className='flex py-1'>
                  <img src={start} className='bg-yellow w-4 h-4 mt-1' alt="ratingIcon" />
                <p className="mt-1 text-sm mx-1 text-gray-500">{product.rating} <span> |</span> <span>{product.terjual}Terjual</span></p>
                </div>
              
                
              </div>
            
            </div>
          </div>
        ))}
        </div>}
        {activeTab === 3 &&
        <div className="mt-6 grid grid-cols-2 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-6 xl:gap-x-8">
        {products.map((product) => (
          <div key={product.id} className="group relative rounded-md bg-white shadow-md">
            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-auto">
              <img
                src={product.imageSrc}
                alt={product.imageAlt}
                className="h-44 w-full object-cover object-center lg:h-44 lg:w-full"
              />
            </div>
            <div className="mt-1 flex justify-between p-2">
              <div>
                <h3 className="text-sm py-1 text-gray-700 line-clamp-2">
                  <a href={product.href}>
                    <span aria-hidden="true" className="absolute inset-0" />
                    {product.name}
                  </a>
                  
                </h3>
                <p className="text-sm py-1 font-bold text-gray-900">{product.price}</p>
                <div className='py-1'>
                  <p className='text-xs px-1 bg-green-300 text-green-600 w-[100px] rounded-md'>{product.coupon}</p>
                </div>
                  <div className='flex'>
                      <img src="https://images.tokopedia.net/img/goldmerchant/pm_activation/badge/Power%20Merchant%20Pro@2x.png" alt="iconPower" className='w-4 h-4' />
                      <p className='text-sm mx-1'>{product.place}</p>
                  </div>
                <div className='flex py-1'>
                  <img src={start} className='bg-yellow w-4 h-4 mt-1' alt="ratingIcon" />
                <p className="mt-1 text-sm mx-1 text-gray-500">{product.rating} <span> |</span> <span>{product.terjual}Terjual</span></p>
                </div>
              
                
              </div>
            
            </div>
          </div>
        ))}
        </div>}
        {activeTab === 4 &&
        <div className="mt-6 grid grid-cols-2 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-6 xl:gap-x-8">
        {products.map((product) => (
          <div key={product.id} className="group relative rounded-md bg-white shadow-md">
            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-auto">
              <img
                src={product.imageSrc}
                alt={product.imageAlt}
                className="h-44 w-full object-cover object-center lg:h-44 lg:w-full"
              />
            </div>
            <div className="mt-1 flex justify-between p-2">
              <div>
                <h3 className="text-sm py-1 text-gray-700 line-clamp-2">
                  <a href={product.href}>
                    <span aria-hidden="true" className="absolute inset-0" />
                    {product.name}
                  </a>
                  
                </h3>
                <p className="text-sm py-1 font-bold text-gray-900">{product.price}</p>
                <div className='py-1'>
                  <p className='text-xs px-1 bg-green-300 text-green-600 w-[100px] rounded-md'>{product.coupon}</p>
                </div>
                  <div className='flex'>
                      <img src="https://images.tokopedia.net/img/goldmerchant/pm_activation/badge/Power%20Merchant%20Pro@2x.png" alt="iconPower" className='w-4 h-4' />
                      <p className='text-sm mx-1'>{product.place}</p>
                  </div>
                <div className='flex py-1'>
                  <img src={start} className='bg-yellow w-4 h-4 mt-1' alt="ratingIcon" />
                <p className="mt-1 text-sm mx-1 text-gray-500">{product.rating} <span> |</span> <span>{product.terjual}Terjual</span></p>
                </div>
              
                
              </div>
            
            </div>
          </div>
        ))}
        </div>}
        {activeTab === 5 &&
        <div className="mt-6 grid grid-cols-2 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-6 xl:gap-x-8">
        {products.map((product) => (
          <div key={product.id} className="group relative rounded-md bg-white shadow-md">
            <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-auto">
              <img
                src={product.imageSrc}
                alt={product.imageAlt}
                className="h-44 w-full object-cover object-center lg:h-44 lg:w-full"
              />
            </div>
            <div className="mt-1 flex justify-between p-2">
              <div>
                <h3 className="text-sm py-1 text-gray-700 line-clamp-2">
                  <a href={product.href}>
                    <span aria-hidden="true" className="absolute inset-0" />
                    {product.name}
                  </a>
                  
                </h3>
                <p className="text-sm py-1 font-bold text-gray-900">{product.price}</p>
                <div className='py-1'>
                  <p className='text-xs px-1 bg-green-300 text-green-600 w-[100px] rounded-md'>{product.coupon}</p>
                </div>
                  <div className='flex'>
                      <img src="https://images.tokopedia.net/img/goldmerchant/pm_activation/badge/Power%20Merchant%20Pro@2x.png" alt="iconPower" className='w-4 h-4' />
                      <p className='text-sm mx-1'>{product.place}</p>
                  </div>
                <div className='flex py-1'>
                  <img src={start} className='bg-yellow w-4 h-4 mt-1' alt="ratingIcon" />
                <p className="mt-1 text-sm mx-1 text-gray-500">{product.rating} <span> |</span> <span>{product.terjual}Terjual</span></p>
                </div>
              
                
              </div>
            
            </div>
          </div>
        ))}
        </div>}

        <div className='py-8 '>
            <div className='flex justify-center'>
            <button className='text-green-500 font-bold  py-3 px-20 rounded-md border-2 border-green-500'>Muat Lebih Banyak</button>
            </div>
        </div>
    
        </div>
        
  )
}

export default ProductListTab