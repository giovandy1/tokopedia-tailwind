import React from 'react'
import Countdown,{ zeroPad} from 'react-countdown';
import ProductCarousel from './ProductCarousel';

const FlashSale = () => {
    const renderer = ({ hours, minutes, seconds, completed }) => {
        if (completed) {
          // Render a completed state
        } else {
          // Render a countdown
          return <div className='flex text-red-500'>
           <span className='text-white bg-rose-500 font-bold p-2 mx-2 rounded-md text-sm'> {zeroPad(hours)}</span>:
           <span className='text-white bg-rose-500 font-bold p-2 mx-2 rounded-md  text-sm'> {zeroPad(minutes)}</span> :
           <span className='text-white bg-rose-500 font-bold p-2 mx-2 rounded-md  text-sm'> {zeroPad(seconds)}</span>

           </div>
        }
      };
  return (
    <div className='container px-10 mx-10 py-3 mt-8 '>
        <div className='flex'>
           <div className='flex '>
            <span className='text-2xl font-bold px-1'> Traktiran Pengguna Baru</span>
            <span className='text-gray-500 px-1 mt-2'>Berakhir dalam</span>
            <span className=''>
            <Countdown date={Date.now() + 10000000}
             renderer={renderer}
            />
                </span> 
            <span className='text-green-500 px-1 mt-2'><a href="#">Lihat Semua</a></span>
           </div>
        </div>
        <div className='my-16'>
        <ProductCarousel/>
        </div>
    </div>
  )
}

export default FlashSale