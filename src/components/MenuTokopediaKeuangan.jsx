import React from 'react'

const MenuTokopediaKeuangan = () => {
  return (
    <div className='container mx-10  h-[400px] '>
    <div className=''>
      <h1 className='font-bold text-2xl'>Tokopedia Keuangan</h1>
    </div>
    <div className=' px-4 grid grid-cols-5 my-6'>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2023/1/25/0496a0d2-7371-45ab-99ec-e446958a0762.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Proteksi</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/6/852f2f1c-64c2-4b9e-8a6c-491db49c9d8d.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Klaim Proteksi</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/8/18/0d7b8ff3-36fa-483c-a2dc-31c63d38d57e.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Emas</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/8/18/2fcff856-4fd4-4a75-b2d1-30eac68535a6.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Reksa Dana</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/8/18/1ce57919-21eb-4246-a403-946cf72b8689.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Dana Instant</p>
        </a>   
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/8/18/3a2a80d9-db1d-4729-85fd-9f95ff79241e.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Modal Toko</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/8/18/ac621c92-553b-4e92-8dc1-2173d5ee5bd8.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Tokopedia Giftcard</p>
        </a>   
    </div>
</div>
  )
}

export default MenuTokopediaKeuangan