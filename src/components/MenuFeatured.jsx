import React from 'react'

const MenuFeatured = () => {
  return (
    <div className='container mx-10  h-[400px] '>
        <div className=''>
          <h1 className='font-bold text-2xl'>Featured</h1>
        </div>
        <div className=' px-4 grid grid-cols-5 my-6'>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2023/9/7/6f1e9e0b-c8df-4aa7-b087-ad2ca11dec57.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Promo</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/5/25/3bde8596-10c7-42c5-bc27-5c08fedaccd0.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Tokopedia Card</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/4/26/97b70c81-f290-45c9-838b-031635a23442.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Uang Elektronik</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2023/9/7/390a1bc2-21f3-4a0a-a7c2-cdea6e4882a6.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Dilayani Tokopedia</p>
            </a>
            <a href='#' className='flex'>
                <img src="https://images.tokopedia.net/img/MIPuRC/2022/4/28/050ba0c1-8f23-46bd-a0e5-8c2d4467c7ec.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Tokopedia Sehat</p>
            </a>   
        </div>
    </div>
  )
}

export default MenuFeatured