import React, { Component } from "react";
import Slider from "react-slick";

export default class CategoriesItem extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4
    };
    return (
      <div>
        <Slider {...settings}>
          <div className="p-4 ">
            <div className="drop-shadow border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          <div className="p-4">
          <div className=" drop-shadow-sm border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          <div className="p-4">
          <div className="drop-shadow-sm border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          <div className="p-4">
          <div className="drop-shadow-sm border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          <div className="p-4">
          <div className="drop-shadow-sm border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          <div className="p-4">
          <div className=" drop-shadow-sm border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          <div className="p-4">
          <div className=" drop-shadow-sm border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          <div className="p-4">
          <div className="drop-shadow-sm border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          <div className="p-4">
          <div className=" border-slate-300 rounded-lg ">
           <img src="https://images.tokopedia.net/img/cache/200/attachment/2018/8/9/3127195/3127195_e5b3e074-c897-4cf0-9ced-5572d0538e7c.jpg.webp" alt="" />
            </div>
          </div>
          
          
        </Slider>
      </div>
    );
  }
}