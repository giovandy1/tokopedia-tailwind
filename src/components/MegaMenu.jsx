import React, { useState } from 'react';
import { Popover } from '@headlessui/react';
import MegaMenuContent from './MegaMenuContent';

const MegaMenu = () => {

    const [activeTab, setActiveTab] = useState(1);

    const handleTabMegaMenuClick = (tabNumber) => {
        setActiveTab(tabNumber);
    };

    return (
        <Popover.Panel className="fixed z-10 bg-white border rounded-md shadow-lg p-4 w-full right-0 mt-7">
            <div className='container mx-10 '>
                <div className='flex mb-4'>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 1 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(1)}
                    >
                        Belanja
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 2 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(2)}
                    >
                        Featured
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 3 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(3)}
                    >
                        Kebutuhan Harian
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 4 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(4)}
                    >
                        Tagihan
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 5 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(5)}
                    >
                        Top-Up
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 6 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(6)}
                    >
                        Tokopedia Keuangan
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 7 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(7)}
                    >
                        Pajak & Pendidikan
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 8 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(8)}
                    >
                        Travel & Entertaiment
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 9 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(9)}
                    >
                        Halal Corner
                    </div>
                    <div
                        className={`cursor-pointer text-gray-500 text-sm px-4 ${activeTab === 10 ? 'active text-green-500' : 'text-gray-500'}`}
                        onClick={() => handleTabMegaMenuClick(10)}
                    >
                        Dan Lain-lain
                    </div>
                </div>

            </div>
            <div className='fixed z-10 bg-white border rounded-md shadow-lg p-4 w-full right-0 py-2'>
            <MegaMenuContent activeTab={activeTab} />
                
            </div>
        </Popover.Panel>
    );
};

export default MegaMenu;
