import React from 'react'
import MenuBelajar from './MenuBelajar';
import MenuFeatured from './MenuFeatured';
import MenuKebutuhan from './MenuKebutuhan';
import MenuTagihan from './MenuTagihan';
import MenuTopUp from './MenuTopUp';
import MenuLainlain from './MenuLainlain';
import MenuTokopediaKeuangan from './MenuTokopediaKeuangan';
import MenuPajakPendidikan from './MenuPajakPendidikan';
import MenuTravel from './MenuTravel';
import MenuHalalCorner from './MenuHalalCorner';
const MegaMenuContent = ({ activeTab }) => {
    return (
        <div>
            {activeTab === 1 && <MenuBelajar/>}
            {activeTab === 2 && <MenuFeatured/>}
            {activeTab === 3 && <MenuKebutuhan/>} 
            {activeTab === 4 && <MenuTagihan/>}
            {activeTab === 5 && <MenuTopUp/>}
            {activeTab === 6 && <MenuTokopediaKeuangan/>}
            {activeTab === 7 && <MenuPajakPendidikan/>}
            {activeTab === 8 && <MenuTravel/>}
            {activeTab === 9 && <MenuHalalCorner/>}
            {activeTab === 10 && <MenuLainlain/>}
        </div>
    )
}

export default MegaMenuContent