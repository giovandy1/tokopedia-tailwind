import React from 'react'

const MenuTagihan = () => {
  return (
    <div className='container mx-10  h-[400px] '>
        <div className=''>
          <h1 className='font-bold text-2xl'>Tagihan</h1>
        </div>
        <div className=' px-4 grid grid-cols-5 my-6'>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/c1027544-6467-4690-a0f0-515e9f1788de.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Air PDAM</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/e45492a9-315c-450f-b297-93adc56006b6.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Angsuran Credit</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/9921b4d5-2092-48ef-ba8b-95d02cd2c9e1.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>BPJS</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/attachment/2019/9/12/47197032/47197032_6bd19146-463f-41e6-81bf-4f14678c1ec1.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>BrideStory Pay</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/d9157549-1162-41cf-a9b6-b967e3f36f2a.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>E-Invoicing</p>
            </a>   
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/38a8d11e-a6ab-40c9-bccb-a40c3b95f797.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Tagihan Gas</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/84586628-7e23-458e-a435-3c5b27da1c70.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Internet & TV Kabel</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/3f3c153c-020e-49ed-a7f5-0550d12da831.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Langganan</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/d3094155-ff40-49a3-9707-72dccaf33142.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Listrik PLN</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/7efc302a-4795-456a-a484-33a8a5f1f45c.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Pascayabar</p>
            </a>  
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/f0fe54b0-9b4a-4e75-b516-9dcd076ba2e5.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Premi Asuransi</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/1e64a9f8-4497-412b-b972-76e65d15f607.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Tagihan IPL</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/6/5a514245-71a6-4b40-a708-7ad00dd68acd.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Tagihan kartu Credit</p>
            </a>
            <a href='#' className=' flex py-2'>
                <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/8603abc9-28a3-4faf-8b5f-9fb731520819.png" alt="" className='h-8 w-8' />
                <p className='text-xs font-bold px-2 py-2'>Telkom </p>
            </a>
        </div>
    </div>
  )
}

export default MenuTagihan