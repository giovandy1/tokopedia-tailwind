import React, { useState } from 'react'
import CategoriesItem from './CategoriesItem'
import ContentCategories from './ContentCategories';
import CategoriesProductCarousel from './CategoriesProductCarousel';

const CategoriesComponent = () => {
    const [activeTab, setActiveTab] = useState(1);

    const handleTabClick = (tabNumber) => {
        setActiveTab(tabNumber);
    };
    return (
        <div className='container mx-10 px-10 my-4 '>
            <div className='bg-white rounded-lg border border-gray-200 shadow-lg  '>
                <div className=' grid grid-cols-2 px-4'>
                    <div className='col-span-1'>
                        <div className='p-4'>
                            <h1 className='font-bold text-xl'>Kategori Pilihan</h1>
                        </div>
                        <div>
                            <CategoriesItem />
                        </div>
                    </div>
                    <div className='col-span-1'>
                        <div className='flex px-4'>
                            <h1 className='mt-4 mx-1 font-bold text-xl'>Top Up & Tagihan</h1>
                            <a href="" className='text-green-400 py-4 mt-2 text-sm font-bold'>Lihat Semua</a>
                        </div>
                        <div className='border rounded-lg mx-4'>
                            <div className="flex justify-between mx-4 ">
                                <div
                                    className={`cursor-pointer p-2 ${activeTab === 1 ? 'bg-white text-green-500 underline underline-offset-8' : 'bg-white-300 text-black'
                                        }`}
                                    onClick={() => handleTabClick(1)}
                                >
                                    Pulsa
                                </div>
                                <div
                                    className={`cursor-pointer p-2 ${activeTab === 2 ? 'bg-white text-green-500 underline underline-offset-8' : 'bg-white-300 text-black'
                                        }`}
                                    onClick={() => handleTabClick(2)}
                                >
                                    Paket Data
                                </div>
                                <div
                                    className={`cursor-pointer p-2 ${activeTab === 3 ? 'bg-white text-green-500 underline underline-offset-8' : 'bg-white-300 text-black'
                                        }`}
                                    onClick={() => handleTabClick(3)}
                                >
                                    BPJS
                                </div>
                                <div
                                    className={`cursor-pointer p-2 ${activeTab === 4 ? 'bg-white text-green-500 underline underline-offset-8' : 'bg-white-300 text-black'
                                        }`}
                                    onClick={() => handleTabClick(4)}
                                >
                                    Listrik PLN
                                </div>
                            </div>
                            <hr />
                            <div className="mt-4">
                                <ContentCategories activeTab={activeTab} />
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <CategoriesProductCarousel/>
                </div>
            </div>
        </div>
    )
}

export default CategoriesComponent