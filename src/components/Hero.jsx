import React, { Component } from "react";
import Slider from "react-slick";

export default class Hero extends Component {
  render() {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };
    return (
      <div className="my-4 container px-10 ">
        <div  className="container mx-10 w-full ">
        <Slider {...settings} >
          <div className=" ">
           <img src="https://images.tokopedia.net/img/cache/1208/NsjrJu/2024/1/10/12582b03-6a31-4ec8-bd4e-3c99e2079ff3.jpg.webp?ect=4g" alt="image-1" className="rounded-lg" />
           <div className="flex justify-end">
           <a href="#" className="absolute bottom-0 left-90 px-2 mx-4  rounded-md mb-4 py-1 text-xs bg-black text-white">lihat promo lainnya</a>
           </div>
           
          </div>
          <div>
           <img src="https://images.tokopedia.net/img/cache/1208/NsjrJu/2024/1/9/947b7293-fe66-43bb-a615-218c6504e413.jpg.webp?ect=4g" alt="image-2" className="rounded-lg" />
           <div className="flex justify-end">
           <a href="#" className="absolute bottom-0 left-90 px-2 mx-4  rounded-md mb-4 py-1 text-xs bg-black text-white">lihat promo lainnya</a>
           </div>
          </div>
          <div>
            <img src="https://images.tokopedia.net/img/cache/1208/NsjrJu/2024/1/8/b54af5a6-3dd2-4ea1-ae78-d72ae64ca57b.jpg.webp?ect=4g" alt="image-3" className="rounded-lg"  />
            <div className="flex justify-end">
           <a href="#" className="absolute bottom-0 left-90 px-2 mx-4  rounded-md mb-4 py-1 text-xs bg-black text-white">lihat promo lainnya</a>
           </div>
          </div>
        </Slider>
      </div>
      </div>
    );
  }
}