import React from 'react'

const MenuPajakPendidikan = () => {
  return (
    <div className='container mx-10  h-[400px] '>
    <div className=''>
      <h1 className='font-bold text-2xl'>Pajak & Pendidikan</h1>
    </div>
    <div className=' px-4 grid grid-cols-5 my-6'>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/8/10/36c6e63b-6329-43a9-930a-b450d5452010.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Pajak Daerah Lainnya</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/64cb09db-3bf9-4721-b86c-461381fdc583.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>E-Samsat</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/6/95f0baea-5d24-4f81-a88d-6ecdd061e82c.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Pajak PBB</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/cache/100-square/attachment/2019/10/14/47197032/47197032_3d2e68e2-a20e-4763-bbef-4abaa7b8ad2c.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Penerimaan Negara</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/8171c4b0-3ae6-4aac-9a4f-6bb88e0e63c2.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Retribusi</p>
        </a>   
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/dc2e4cfd-4d33-4ffa-9dbb-6c80317b9b79.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Prakerja</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/8/7/ec5e1986-cc7a-40c0-a3a1-36c34821beb4.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Biaya  Pendidikan</p>
        </a>
        <a href='#' className='flex py-2'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2022/1/10/bcbb013f-dc6c-4c2b-8bf4-b11844d38b7d.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Belajar</p>
        </a>   
    </div>
</div>
  )
}

export default MenuPajakPendidikan