import React from 'react'

const MenuKebutuhan = () => {
  return (
    <div className='container mx-10  h-[400px] '>
    <div className=''>
      <h1 className='font-bold text-2xl'>Kebutuhan Harian</h1>
    </div>
    <div className=' px-4 grid grid-cols-5 my-6'>
        <a href='#' className='flex'>
            <img src="https://images.tokopedia.net/img/MIPuRC/2023/6/20/d2f80def-fbb5-4fab-ac1c-499f774687af.png" alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Ibu & Bayi</p>
        </a>
        <a href='#' className='flex'>
            <img src="https://images.tokopedia.net/img/cache/100-square/MIPuRC/2021/5/28/d934b056-bbd8-49ab-b0d8-24608a6029a6.png   " alt="" className='h-8 w-8' />
            <p className='text-xs font-bold px-2 py-2'>Perawatan Hewan</p>
        </a>
       
    </div>
</div>
  )
}

export default MenuKebutuhan