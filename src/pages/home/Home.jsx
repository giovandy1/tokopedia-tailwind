import React from 'react'
import Navbar from '../../components/Navbar'
import Hero from '../../components/Hero'
import CategoriesComponent from '../../components/CategoriesComponent'
import FlashSale from '../../components/FlashSale';
import ProductList from '../../components/ProductList';
import TabCategories from '../../components/TabCategories';

const Home = () => {
    return (
        <div className='w-full'>
            <Navbar />
            <Hero/>
            <CategoriesComponent/>
            <FlashSale/>
            <ProductList/>
            <TabCategories/>
        </div>
    )
}

export default Home